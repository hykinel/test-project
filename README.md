Good points :+1:

1. Re: UP org recognition
    * There was a time from 2007 to 2014? na official UP org ang UPPG
    * The setup was:
        1. Quiwarriors - ICPC trainees / contestants
            * Around 9 to 15 members, weekday lectures, Saturday mock, etc.
            * Pretty much similar to what we have now
        2. Qode Weavers - non-ICPC members
            * Manage/develop websites, notable ones: [CRS](https://crs.upd.edu.ph/) and [Bukluran](http://bukluran.osa.upd.edu.ph/)
            * Nag-start kasi ang UPPG na all ComSci majors, eto ung mga friends ng mga CS peeps na hinatak nila para maabot ung minimum head count for org recognition
            * Other tasks: usual org stuff (pub mat, ACLE, etc.)
        3. [Early history](http://iskwiki.upd.edu.ph/index.php/UP_Programming_Guild)
    * Eventually nag-move ung demographic from
        * (CS-only) to (CS-majority + some EEE) to (EEE-majority + some CS + stray Math,Phys,CE,ChE,ME) to (EEE-majority + stray CS,Math,Phys,Engg)
        * Na-rereplenish yung population ng Quiwarriors pero walang pumalit sa Qodeweavers
    * Mas madali rin yung pag-register ng UPPG dati kasi yung web-admins sa Bukluran (UP org registration system) ay puro Qodeweavers :see_no_evil:
    * Lumiit lang yung population over time tas di na naabot yung minimum requirement for org recognition
2. Re: elimination/culling of members
    * Yung constraint talaga dito ay yung budget for joining contests abroad
        * Usually 1 team (sometimes 2, and very rarely 3) lang pinapadala for SEA regionals
        * Pag nag-host ang Manila ng ICPC regionals, kaya ang 5 teams pasalihin easily
        * Balik sa ADMU yung ICPC Manila regionals on Dec 2019
    * Kaya ideal yung 9 people (3 teams) kasi realistically eto lang yung mga makakakuha ng actual ICPC "playing time"
    * Pwede pa rin i-consider ang members #10 to #15 pero magiging "bench players" sila at makakasali lang kung may local contests
    * Ideal contestant pool: 9, ideal training pool: 15
3. Re: training structure/organization/transparency
    * I think these are all valid arguments and we could definitely improve on these aspects
    * Side note: if you're familiar with the DotA 2 pro scene, these concerns are eerily similar
        1. Valve and lack of transparency regarding tournament invites:
            * https://www.reddit.com/r/DotA2/comments/7t5qpg/valve_we_need_transparent_rules_around_major/
            * https://www.reddit.com/r/DotA2/comments/4ow754/valve_should_explain_how_they_invite_teams_for_ti/
            * http://blog.dota2.com/2016/04/manila-major-invites/
        2. Valve and lack of communication
            * Fresh spicy drama: https://www.reddit.com/r/DotA2/comments/a3217t/reminder_that_valves_lack_of_communication_is/
        3. Valve and lack of clear rules / policing
            * https://www.reddit.com/r/DotA2/comments/8cpg5b/i_really_hope_valve_steps_in_and_makes_some_clear/
            * https://www.reddit.com/r/DotA2/comments/a1j6ao/we_need_a_community_manager_and_clear_rules_valve/
            * Fresh spicy drama: https://www.reddit.com/r/DotA2/comments/a3b62g/the_problem_with_valves_statement_is_part_of/
    * I think Valve has addressed issue #1 with the introduction of the DPC:
        * http://blog.dota2.com/2017/09/the-dota-pro-circuit/
        * http://blog.dota2.com/2018/06/the-dota-pro-circuit-2018-2019/
        * Pwede nating gayahin to, may corresponding points earned from your scores in Mock compets, UVa/Kattis/Euler problems solved, etc.
    * Re: #2 and #3 I think from the very start naging Laissez faire na yung culture within UPPG
    * Fun fact: ang founding president ng UPPG ay anarchist (the political/ideological kind) :rofl:
    * Mas mahirap eto i-solve kasi free-form ang problem unlike team selection / DPC points tracking, but you did raise good points in the google doc:
        1. Plan the training year in advance
        2. Set rules on attendance and participation
        3. Earlier announcements for events
4. Moving forward
    1. Mas ok nga kung official UP org dahil mas madali ma-access ang UP resources (i.e. room usage, fundraising, pirma for excuse letter, etc.)
    2. We'll set up a DPC-like system effective next year
        * Merong rating/ranking system ang NUS na ginagamit ni Halim for team selection: https://www.comp.nus.edu.sg/~stevenha/
            * SH7 ranking: https://www.comp.nus.edu.sg/~stevenha/icpc.html
            * Di uso sa kanila ang Coder,Algo,Math sub-roles so top3=team1, top4-6=team2, ...
            * Lahat ng contestants ay all-rounder, so mas consideration nila ang pag-group ng similar year-level (para sabay mag-grow/graduate ang team), similar nationality (for team chemistry / communication)
        * We'll need a thorough discussion re: the criteria and retroactive nature of this points system
    3. Buhayin ulit ang Qode Weavers
        * Suggestion: instead of eliminating members, we "demote" them to Qode Weaver status
        * This way, we can keep the head count up for org recog
        * The Qodeweavers can then do software development projects on the side
    4. UPPG website
        * Matagal ko na iniisip gawin to as pet project after work hours, but dota/kpop/anime is life
        * I'll start working on this during the Christmas break. If we decited to re-add the Qodeweavers, they can do parallel work on separate features next year.
        * It could house the following:
            * Rank list for TBD UPPG points system
            * Announcements
            * Blog site
            * Calendar of activities
            * Rules and regulations
